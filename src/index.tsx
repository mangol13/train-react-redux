import React from 'react';
import ReactDOM from 'react-dom';
import { Provider as ReduxProvider } from 'react-redux';
import { ErrorBoundary } from 'react-error-boundary';

// import 'reflect-metadata';
import App from './app/App';
import { store } from '~/app/redux/store';
import { MyErrorBoundary } from '~/app/components/err/MyErrorBoundary';
import { Error as ErrorC } from './app/components/err/Error';

const handleError = (error: Error) => {
  sessionStorage.setItem('error', error.message);
  window.location.href = '/error';
};

ReactDOM.render(
  <ErrorBoundary onError={handleError} fallbackRender={ErrorC}>
    <ReduxProvider store={store}>
      <App />
    </ReduxProvider>
  </ErrorBoundary>,
  document.getElementById('root')
);
