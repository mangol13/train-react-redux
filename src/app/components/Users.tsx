import React, { useEffect, useState, useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';

import { usersSelector } from '~/app/redux/selectors/user-selector';

import { getUsers, User } from '~/app/service/user-service';
import { setUsers } from '~/app/redux/actions/user-actions';
import { deleteUser } from '~/app/redux/thunk/user-thunk';

import { Loading } from '~/app/components/Loading';

function UserC({ user: { id, lastName, name } }: { user: User }) {
  const dispatch = useDispatch();
  const history = useHistory();
  const path = `/user/${id}`;

  const handleEdit = (e: any) => {
    e.preventDefault();
    history.push(path);
  };

  const handleDelete = (e: any) => {
    e.preventDefault();
    dispatch(deleteUser(id));
  };

  return (
    <li>
      <div>{id}</div>
      <div>{name}</div>
      <div>{lastName}</div>
      <div>
        <a href={path} onClick={handleEdit}>
          Edit
        </a>
      </div>
      <div>
        <a href="#" onClick={handleDelete}>
          Delete
        </a>
      </div>
    </li>
  );
}

export default function Users() {
  const dispatch = useDispatch();

  useEffect(() => {
    console.log('====mounted');
    getUsers().then((users) => {
      console.log(users);
      dispatch(setUsers(users));
    });
  }, []);

  const users = useSelector(usersSelector);

  return !users ? (
    <Loading />
  ) : (
    <div className="users">
      <h2>Users</h2>
      <ul>
        {users.map((u) => (
          <UserC key={u.id} user={u} />
        ))}
      </ul>
    </div>
  );
}
