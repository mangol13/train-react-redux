import React from 'react';

export function Error() {
  const error = sessionStorage.getItem('error');
  return <h1>Error: {error}</h1>;
}
