import React from 'react';
import {} from 'react-router-dom';

type State = {
  hasError: boolean;
};

export class MyErrorBoundary extends React.Component {
  public state: State = { hasError: false };

  constructor(props: any) {
    super(props);
  }

  public static getDerivedStateFromError(error: any) {
    // Update state so the next render will show the fallback UI.
    return { hasError: true };
  }

  public componentDidCatch(error: any, errorInfo: any) {
    // You can also log the error to an error reporting service
    // logErrorToMyService(error, errorInfo);
    console.log('=====Error', { error, errorInfo });
    sessionStorage.setItem('error', error);

    window.location.href = '/error';
    // window.history.pushState({}, '', '/error');
  }

  public render() {
    if (this.state.hasError) {
      // You can render any custom fallback UI
      return <h1>Something went wrong.</h1>;
    }

    return this.props.children;
  }
}
