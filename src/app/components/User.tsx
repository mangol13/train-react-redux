import React, { useCallback, useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useParams } from 'react-router-dom';
import { isNil } from 'ramda';

import { userSelector } from '~/app/redux/selectors/user-selector';

import { getUsers, User } from '~/app/service/user-service';
import { setUsers } from '~/app/redux/actions/user-actions';
import { Loading } from '~/app/components/Loading';
import { fetchUser, saveUser } from '~/app/redux/thunk/user-thunk';

export default function UserC() {
  const dispatch = useDispatch();
  const { id }: any = useParams();

  useEffect(() => {
    console.log('======mounted');
    dispatch(fetchUser(parseInt(id)));
  }, []);

  const user = useSelector(userSelector);

  const [name, setName] = useState(user?.name);
  const [lastName, setLastName] = useState(user?.lastName);
  const [readOnly, setReadonly] = useState(true);

  const nameValue = isNil(name) ? user?.name : name;
  const lastNameValue = isNil(lastName) ? user?.lastName : lastName;

  const handleChangeName = useCallback(
    (e: any) => {
      setName(e.target.value);
    },
    [name]
  );

  const handleChangeLastName = useCallback(
    (e: any) => {
      setLastName(e.target.value);
    },
    [lastName]
  );

  const handleToggleEdit = useCallback(
    (e: any) => {
      setReadonly(!readOnly);
    },
    [readOnly]
  );

  const handleSubmit = useCallback(
    (e: any) => {
      e.preventDefault();
      setReadonly(true);
      dispatch(
        saveUser({
          id: user.id,
          name: nameValue,
          lastName: lastNameValue,
        })
      );
    },
    [nameValue, lastNameValue]
  );

  return !user ? (
    <Loading />
  ) : (
    <div>
      <h2>User id={user.id}</h2>
      <form onSubmit={handleSubmit}>
        <div>
          <label>name:</label>
          <input type="text" value={nameValue} onChange={handleChangeName} readOnly={readOnly} disabled={readOnly} />
        </div>
        <div>
          <label>lastName:</label>
          <input type="text" value={lastNameValue} onChange={handleChangeLastName} readOnly={readOnly} disabled={readOnly} />
        </div>
        <div>
          {readOnly && <button onClick={handleToggleEdit}>edit</button>}
          {!readOnly && <button onClick={handleToggleEdit}>cancel</button>}
          <button disabled={readOnly}>save</button>
        </div>
      </form>
    </div>
  );
}
