import React, { Suspense } from 'react';
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';
import { Loading } from '~/app/components/Loading';
// import Login from './Login';
import Users from './Users';
import User from './User';
import { Error } from '~/app/components/err/Error';
import { Auth } from '~/app/components/Auth';

const Login = React.lazy(() => import('./Login'));

export default function AppRouter() {
  return (
    <BrowserRouter>
      <Auth>
        <Suspense fallback={<Loading />}>
          <Switch>
            <Route exact={true} path="/" render={() => <Redirect to="/login" />} />
            <Route exact={true} path="/login" component={Login} />;
            <Route exact={true} path="/users" component={Users} />;
            <Route exact={true} path="/user/:id" component={User} />;
            <Route exact={true} path="/error" component={Error} />;
          </Switch>
        </Suspense>
      </Auth>
    </BrowserRouter>
  );
}
