import React, { Fragment, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { usernameSelector } from '~/app/redux/selectors/auth-selector';
import { useHistory } from 'react-router-dom';

const publicPaths = ['/', '/login'];

export const Auth = (props: any) => {
  const history = useHistory();
  const authUsername = useSelector(usernameSelector);

  console.log('auth', authUsername);
  console.log('path', history.location.pathname);

  useEffect(() => {
    const path = history.location.pathname;
    if (!authUsername && !publicPaths.includes(path)) {
      history.push('/403');
    }
  });

  return <Fragment>{props.children}</Fragment>;
};
