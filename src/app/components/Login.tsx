import React, { FC, useState, useEffect, useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { useTranslation } from 'react-i18next';

import { logged } from '~/app/redux/actions/auth-actions';
import { usernameSelector } from '~/app/redux/selectors/auth-selector';

type Properties = {
  label: string;
  value: string;
  onchange: (value: string) => void;
  // onchange: (e: React.ChangeEvent<HTMLInputElement>) => any;
  setFormError: (field: string, value: string | boolean) => void;
};

const isError = (v: string) => v.trim() === '';

const RawForm: FC<Properties> = ({ label, value, onchange, setFormError }) => {
  const [touched, setTouched] = useState(false);

  // if (!touched) {
  //   setFormError(label, isError(value));
  // }

  const handleChange = useCallback((e: React.ChangeEvent<HTMLInputElement>) => {
    setTouched(true);
    const newValue = e.target.value;
    setFormError(label, isError(newValue));
    onchange(newValue);
  }, []);

  return (
    <div className={`raw-form ${touched && isError(value) ? 'err' : ''} `}>
      <label>{label}:</label>
      <input type="text" value={value} onChange={handleChange} />
    </div>
  );
};

export default function Login(props: any) {
  console.log('props:', props);
  // just for testing componentDidMountEffect
  useEffect(() => {
    console.log('====mounted');
  }, []);

  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [formErrors, setFormErrors] = useState({});
  const [loginErr, setLoginErr] = useState(false);

  const dispatch = useDispatch();
  const authUsername = useSelector(usernameSelector);
  console.log('authUsername', authUsername);

  const history = useHistory();
  const [t] = useTranslation();

  const isFormInvalid = () => !!Object.values(formErrors).find((o) => o);
  const setFormError = (field: string, value: string | boolean) => {
    setFormErrors({
      ...formErrors,
      [field]: value,
    });
  };

  const handleSubmit = useCallback(
    (e: any) => {
      e.preventDefault();
      if (username === 'user' && password === 'pass') {
        setLoginErr(false);
        dispatch(logged(username));
        history.push('/users');
      } else {
        setLoginErr(true);
      }
    },
    [username, password]
  );

  return (
    <div className="loginForm">
      <form onSubmit={handleSubmit}>
        <RawForm label={t('username')} value={username} onchange={(value) => setUsername(value)} setFormError={setFormError} />
        <RawForm label="password" value={password} onchange={(value) => setPassword(value)} setFormError={setFormError} />
        {loginErr && <div className="err">Invalid credentials</div>}
        <div style={{ textAlign: 'center' }}>
          <button disabled={isFormInvalid()}>Login</button>
        </div>
      </form>
    </div>
  );
}
