import { UserActionTypes } from './action-types';
import { User } from '~/app/service/user-service';

export const setUsers = (users: User[]) => ({
  type: UserActionTypes.SET_USERS,
  payload: { users },
});

export const setUser = (user: User) => ({
  type: UserActionTypes.SET_USER,
  payload: { user },
});
