import { AuthActionTypes } from './action-types';

export const logged = (username: string) => ({
  type: AuthActionTypes.LOGGED,
  payload: { username },
});
