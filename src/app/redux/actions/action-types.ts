export enum AuthActionTypes {
  LOGGED = 'AUTH.LOGGED',
}

export enum UserActionTypes {
  SET_USERS = 'USER.SET_USERS',
  SET_USER = 'USER.SET_USER',
}
