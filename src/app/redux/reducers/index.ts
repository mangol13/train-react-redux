import { combineReducers } from 'redux';
// import { combineReducers } from '@reduxjs/toolkit';

import authReducer from './auth-reducer';
import userReducer from './user-reducer';

const rootReducer = combineReducers({
  auth: authReducer,
  user: userReducer,
});

// export default {
//   auth: authReducer,
// };

export default rootReducer;

export type RootState = ReturnType<typeof rootReducer>;
