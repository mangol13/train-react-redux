import { UserActionTypes } from './../actions/action-types';
import { Action, createReducer } from './helpers';
import { User } from '~/app/service/user-service';

export type State = {
  users: User[];
  user: User;
};

const initialState: State = {
  users: undefined,
  user: undefined,
};

export default createReducer(initialState, {
  [UserActionTypes.SET_USERS](state: State, { payload }: Action) {
    const { users } = payload;
    return { ...state, users };
  },
  [UserActionTypes.SET_USER](state: State, { payload }: Action) {
    const { user } = payload;
    return { ...state, user };
  },
});
