import { AuthActionTypes } from './../actions/action-types';
import { Action, createReducer } from './helpers';

type State = {
  token: string;
};

const initialState: State = {
  token: undefined,
};

export default createReducer(initialState, {
  [AuthActionTypes.LOGGED](state: State, { payload }: Action) {
    const { username } = payload;
    const token = btoa(JSON.stringify({ username, logged: true }));

    return { ...state, token };
  },
});
