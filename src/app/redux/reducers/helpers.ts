export type Action = {
  type: string;
  payload: any;
};

export function createReducer(initialState: any, handlers: any) {
  return function reducer(state = initialState, action: Action) {
    const handler = handlers[action.type];
    return handler ? handler(state, action) : state;
  };
}
