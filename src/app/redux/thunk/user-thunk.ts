import { users, apiCall, User } from '~/app/service/user-service';
import { setUser, setUsers } from '~/app/redux/actions/user-actions';

export function fetchUser(id: number) {
  const user = users.find((u) => u.id === id);
  if (!user) {
    throw new Error('Invalid user');
  }

  return async function (dispatch: any) {
    const result = await apiCall(user);
    dispatch(setUser(result));
  };
}

export function deleteUser(id: number) {
  console.log('deleteUser', id);
  const idx = users.findIndex((u) => u.id === id);
  if (idx < 0) {
    throw new Error('Invalid user');
  }

  users.splice(idx, 1);

  return async function (dispatch: any) {
    await dispatch(setUsers(undefined));
    const result = await apiCall(users);
    dispatch(setUsers([...users]));
  };
}

export function saveUser(user: User) {
  const idx = users.findIndex((u) => u.id === user.id);
  if (idx < 0) {
    throw new Error('Invalid user');
  }

  const newUser = { ...users[idx], ...user };
  console.log('newUser', newUser);
  users[idx] = newUser;

  return async function (dispatch: any) {
    await dispatch(setUser(undefined));
    const result = await apiCall(newUser);
    dispatch(setUser(newUser));
  };
}
