import { prop, path, isEmpty } from 'ramda';
import { isNilOrEmpty } from 'ramda-adjunct';
import { createSelector } from 'reselect';
import { User } from '~/app/service/user-service';
import { RootState } from '~/app/redux/store';
import { State as UserState } from '~/app/redux/reducers/user-reducer';

const userRootSelector: (state: RootState) => UserState = prop('user');
const users: (state: UserState) => User[] = prop('users');
const user: (state: UserState) => User = prop('user');

export const usersSelector: (state: RootState) => User[] = createSelector(userRootSelector, users);
export const userSelector: (state: RootState) => User = createSelector(userRootSelector, user);
