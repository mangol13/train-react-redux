import { prop, path, isEmpty } from 'ramda';
import { isNilOrEmpty } from 'ramda-adjunct';
import { createSelector } from 'reselect';

const authSelector = prop('auth');
const tokenSelector: (state: any) => string = path(['auth', 'token']);

const tokenSelector2 = (state: any): string => state.auth?.token;

export const usernameSelector = createSelector(tokenSelector, (token: string) => {
  if (isNilOrEmpty(token)) {
    return null;
  }

  console.log('token=', token);

  const tokenDecoded = atob(token);
  const { username } = JSON.parse(tokenDecoded);
  return username;
});
