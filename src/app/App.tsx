import React from 'react';
import '../styles/app.css';
import { Header } from './layout';
import Login from '~/app/components/Login';
import AppRouter from './components/AppRouter';
import '~/i18n';

export default function App() {
  return (
    <div>
      <Header />
      <AppRouter />
    </div>
  );
}
