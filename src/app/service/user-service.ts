export type User = {
  id: number;
  name: string;
  lastName: string;
};

export const users: User[] = Array.from(Array(5).keys()).map((idx) => ({
  id: idx,
  name: `name${idx}`,
  lastName: `lastName${idx}`,
}));

export function apiCall<T>(result: T, delay = 800): Promise<T> {
  return new Promise((res, rej) => {
    setTimeout(() => {
      res(result);
    }, delay);
  });
}

export async function getUsers(): Promise<User[]> {
  const result = await apiCall<User[]>(users);
  return [...result];
}
